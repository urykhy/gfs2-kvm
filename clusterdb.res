resource clusterdb {
    protocol C;
    disk {
        on-io-error detach;
    }
    net {
        allow-two-primaries;
        cram-hmac-alg   sha1;
        shared-secret   123456;
        after-sb-0pri   discard-least-changes;
        after-sb-1pri   violently-as0p;
        after-sb-2pri   violently-as0p;
        rr-conflict violently;
    }
    startup {
        become-primary-on both;
    }
    syncer {
        rate 100M;
    }
    on centos7-1 {
        device /dev/drbd0;
        disk /dev/vda5;
        address 10.103.10.201:7788;
        flexible-meta-disk internal;
    }
    on centos7-2 {
        device /dev/drbd0;
        disk /dev/vda5;
        address 10.103.10.202:7788;
        flexible-meta-disk internal;
    }
}
