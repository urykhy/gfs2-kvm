
all: cluster

gfs/centos7: # centos7.json
		packer build centos7.json

centos7-0: gfs/centos7
		qemu-img create -f qcow2 -b gfs/centos7 centos7-0
		./kvm.py up centos7-0
		ansible-playbook packages.yml
		./kvm.py down centos7-0

centos7-1: centos7-0
		qemu-img create -f qcow2 -b centos7-0 centos7-1
		./kvm.py up centos7-1
		ansible-playbook common.yml --extra-vars="host=centos7-1"
centos7-2: centos7-0
		qemu-img create -f qcow2 -b centos7-0 centos7-2
		./kvm.py up centos7-2
		ansible-playbook common.yml --extra-vars="host=centos7-2"

cluster: centos7-1 centos7-2
		ansible-playbook cluster.yml
		touch $@
